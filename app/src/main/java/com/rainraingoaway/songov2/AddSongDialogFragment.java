package com.rainraingoaway.songov2;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class AddSongDialogFragment extends DialogFragment implements View.OnClickListener {

    private EditText inputSong;
    private EditText inputArtist;
    private TextInputLayout inputSongLayout;
    private TextInputLayout inputArtistLayout;
    private TextView genreTextview;
    private TextView genreNotChosen;
    int selectedGenre = -1;

    public AddSongDialogFragment() { }

    public interface MyInterface {
        void onChoose(String parameters);
    }

    private MyInterface mListener;

    @Override
    public void onAttach(Activity activity) {
        mListener = (MyInterface) activity;
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.addsong_layout, container);
        inputSong = (EditText) view.findViewById(R.id.input_song);
        inputArtist = (EditText) view.findViewById(R.id.input_artist);
        inputSongLayout = (TextInputLayout) view.findViewById(R.id.input_song_layout);
        inputArtistLayout = (TextInputLayout) view.findViewById(R.id.input_artist_layout);
        genreTextview = (TextView) view.findViewById(R.id.genreTextView);
        genreNotChosen = (TextView) view.findViewById(R.id.tv_genre_not_chosen);
        inputSong.addTextChangedListener(new MyTextWatcher(inputSongLayout, inputSong, R.string.err_msg_songname));
        inputArtist.addTextChangedListener(new MyTextWatcher(inputArtistLayout, inputArtist, R.string.err_msg_artistname));
        view.findViewById(R.id.button2).setOnClickListener(this);
        view.findViewById(R.id.ll_genre_menu).setOnClickListener(this);
        genreTextview.setText(R.string.press_to_choose_genre);
        setHasOptionsMenu(true);
        return view;
    }

    public void onClick(View v){
        switch(v.getId()) {
            case R.id.button2:
                if(validateText(inputSongLayout, inputSong, getString(R.string.err_msg_songname))
                        && validateText(inputArtistLayout, inputArtist, getString(R.string.err_msg_artistname))) {
                    if (selectedGenre == -1)
                        genreNotChosen.setVisibility(View.VISIBLE);
                    else {
                        StringBuilder parametersBuilder = new StringBuilder("genreid=");
                        parametersBuilder.append(String.valueOf(selectedGenre));
                        parametersBuilder.append("&song=");
                        parametersBuilder.append(inputArtist.getText().toString());
                        parametersBuilder.append(" - ");
                        parametersBuilder.append(inputSong.getText().toString());
                        mListener.onChoose(parametersBuilder.toString());
                        dismiss();
                    }
                } else if (selectedGenre == -1)
                    genreNotChosen.setVisibility(View.VISIBLE);
                break;
            case R.id.ll_genre_menu:
                Context wrapper = new ContextThemeWrapper(getContext(), R.style.popupMenuStyle);
                final PopupMenu popup = new PopupMenu(wrapper, v);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.song_genres, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch(item.getItemId())
                        {
                            case R.id.pop:
                                selectedGenre = 0;
                                break;
                            case R.id.rock:
                                selectedGenre = 1;
                                break;
                            case R.id.hiphop:
                                selectedGenre = 2;
                                break;
                            case R.id.metal:
                                selectedGenre = 3;
                                break;
                            case R.id.electro:
                                selectedGenre = 4;
                                break;
                            case R.id.alternative:
                                selectedGenre = 5;
                                break;
                            case R.id.punk:
                                selectedGenre = 6;
                                break;
                            case R.id.dance:
                                selectedGenre = 7;
                                break;
                            case R.id.classic:
                                selectedGenre = 8;
                                break;
                            case R.id.bard:
                                selectedGenre = 9;
                                break;
                            case R.id.countrymusic:
                                selectedGenre = 10;
                                break;
                            case R.id.raggy:
                                selectedGenre = 11;
                                break;
                            case R.id.jazz:
                                selectedGenre = 12;
                                break;
                            case R.id.ost:
                                selectedGenre = 13;
                                break;
                        };
                        genreTextview.setText(item.getTitle());
                        genreNotChosen.setVisibility(View.INVISIBLE);
                        return false;
                    }
                });
                popup.show();
                break;
        }
    }

    private boolean validateText(TextInputLayout inputLayout, EditText editText, String errorText) {
        if (editText.getText().toString().trim().isEmpty()) {
            inputLayout.setError(errorText);
            editText.requestFocus();
            return false;
        } else {
            inputLayout.setErrorEnabled(false);
        }

        return true;
    }

    private class MyTextWatcher implements TextWatcher {
        TextInputLayout inputLayout;
        EditText editText;
        String errorText;

        private MyTextWatcher(TextInputLayout inputLayout, EditText editText, int errorStringID)
        {
            this.inputLayout = inputLayout;
            this.editText = editText;
            errorText = getString(errorStringID);
        }
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            validateText(inputLayout, editText, errorText);
        }
    }
}
