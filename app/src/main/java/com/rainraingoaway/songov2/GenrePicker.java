package com.rainraingoaway.songov2;

import android.content.Context;

public final class GenrePicker {
    public static String getGenreName(Context context, int genreID)
    {
        switch (genreID)
        {
            case 0:
                return context.getString(R.string.pop);
            case 1:
                return context.getString(R.string.rock);
            case 2:
                return context.getString(R.string.hiphop);
            case 3:
                return context.getString(R.string.metal);
            case 4:
                return context.getString(R.string.electro);
            case 5:
                return context.getString(R.string.alternative);
            case 6:
                return context.getString(R.string.punk);
            case 7:
                return context.getString(R.string.dance);
            case 8:
                return context.getString(R.string.classic);
            case 9:
                return context.getString(R.string.bard);
            case 10:
                return context.getString(R.string.countrymusic);
            case 11:
                return context.getString(R.string.raggy);
            case 12:
                return context.getString(R.string.jazz);
            case 13:
                return context.getString(R.string.ost);
        }
        return null;
    }

    public static int getGenreId(int viewId)
    {
        switch (viewId)
        {
            case R.id.pop_checkbox:
                return 0;
            case R.id.rock_checkbox:
                return 1;
            case R.id.hiphop_checkbox:
                return 2;
            case R.id.metal_checkbox:
                return 3;
            case R.id.electro_checkbox:
                return 4;
            case R.id.alternative_checkbox:
                return 5;
            case R.id.punk_checkbox:
                return 6;
            case R.id.dance_checkbox:
                return 7;
            case R.id.classic_checkbox:
                return 8;
            case R.id.bard_checkbox:
                return 9;
            case R.id.countrymusic_checkbox:
                return 10;
            case R.id.raggy_checkbox:
                return 11;
            case R.id.jazz_checkbox:
                return 12;
            case R.id.ost_checkbox:
                return 13;
        }
        return -1;
    }

    public static int getCheckboxId(int viewId)
    {
        switch (viewId)
        {
            case 0:
                return R.id.pop_checkbox;
            case 1:
                return R.id.rock_checkbox;
            case 2:
                return R.id.hiphop_checkbox;
            case 3:
                return R.id.metal_checkbox;
            case 4:
                return R.id.electro_checkbox;
            case 5:
                return R.id.alternative_checkbox;
            case 6:
                return R.id.punk_checkbox;
            case 7:
                return R.id.dance_checkbox;
            case 8:
                return R.id.classic_checkbox;
            case 9:
                return R.id.bard_checkbox;
            case 10:
                return R.id.countrymusic_checkbox;
            case 11:
                return R.id.raggy_checkbox;
            case 12:
                return R.id.jazz_checkbox;
            case 13:
                return R.id.ost_checkbox;
        }
        return -1;
    }
}
