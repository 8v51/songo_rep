package com.rainraingoaway.songov2;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPmaster {
    public static class postTask extends AsyncTask<String, Void, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                String urlParameters = params[1];
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                dStream.writeBytes(urlParameters);
                dStream.flush();
                dStream.close();
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                StringBuilder responseOutput = new StringBuilder();
                while((line = br.readLine()) != null ) {
                    responseOutput.append(line);
                }
                br.close();
                Log.d("songo_response", responseOutput.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static class getAlbumArt extends AsyncTask<Void, Void, String> {

        HttpURLConnection getMBIDconnection = null;
        BufferedReader reader = null;
        private ImageView imageView;
        String songInfo;
        boolean success = true;
        final String API_KEY = "7534579874a955113447943eede61f37";

        public getAlbumArt(String songInfo, ImageView imageView) {
            this.imageView = imageView;
            this.songInfo = songInfo;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //метод работает в UI-потоке
        }

        @Override
        protected String doInBackground(Void... params) {
            String albumArtURL = "no_url";
            try {
                songInfo = songInfo.replaceAll(" ", "%20");
                String[] partsOfTrack = songInfo.split("%20-%20");
                URL url = new URL("http://ws.audioscrobbler.com/2.0/?method=artist.getInfo&artist="
                        + partsOfTrack[0]
                        + "&api_key="
                        + API_KEY
                        + "&format=json");
                getMBIDconnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = getMBIDconnection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                final StringBuilder sb = new StringBuilder();
                while ((line = reader.readLine()) != null) { sb.append(line); }

                JSONObject dataJsonObj;

                try {
                    dataJsonObj = new JSONObject(sb.toString());
                    JSONArray images = dataJsonObj.getJSONObject("artist").getJSONArray("image");
                    for (int i = 0; i < images.length(); i++) {
                        JSONObject image = images.getJSONObject(i);
                        if (image.getString("size").equals("large"))
                        {
                            albumArtURL = image.getString("#text");
                            break;
                        }
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return albumArtURL;
        }

        @Override
        protected void onPostExecute(String imageurl) {
            super.onPostExecute(imageurl);
            if(success)
                Picasso.with(imageView.getContext())
                        .load(imageurl)
                        .into(imageView);

        }
    }
}
