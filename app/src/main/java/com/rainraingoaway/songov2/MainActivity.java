package com.rainraingoaway.songov2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements AddSongDialogFragment.MyInterface {
    CoordinatorLayout layout;
    RecyclerViewEmptySupport  mRecyclerView;
    private RecyclerAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    DBHelper dbHelper;
    public ArrayList<Song> data = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        layout = (CoordinatorLayout)findViewById(R.id.coordinator1);
        FloatingActionButton myFab = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        mRecyclerView = (RecyclerViewEmptySupport) findViewById(R.id.my_recycler_view);
        mRecyclerView.setEmptyView(findViewById(R.id.list_empty));
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                AddSongDialogFragment addSongDialogFragment = new AddSongDialogFragment();
                addSongDialogFragment.show(fm, "add_song");
            }
        });
        mAdapter = new RecyclerAdapter(MainActivity.this, data, mRecyclerView);
        mLayoutManager = new LinearLayoutManager(MainActivity.this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        dbHelper = new DBHelper(this);
        mAdapter.swap(dbHelper.readFromDB());
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(700);
        itemAnimator.setRemoveDuration(1000);
        mRecyclerView.setItemAnimator(itemAnimator);
    }

    public void onClick(View v) {
        if (v.getId() == R.id.profile_button) {
            Intent userInfoIntent = new Intent(this, UserInfo.class);
            startActivity(userInfoIntent);
        }
    }
    @Override
    public void onChoose(String postQueryParameters) {
        postQueryParameters += "&userid=";
        SharedPreferences prefs = this.getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        postQueryParameters += prefs.getString("UserID", null);
        String getQueryParameters = prefs.getString("UserID", null);
        new parseTask(getQueryParameters).execute();
        new HTTPmaster.postTask().execute("http://songo.me.pn/db_CreateRowSongs.php", postQueryParameters);
    }

    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            // конструктор суперкласса
            super(context, "myDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table songtable ("
                    + "songname text primary key, "
                    + "songgenreid integer, "
                    //+ "userid text,"
                    + "songuserinfo text, "
                    //+ "country text"
                    //+ "age integer"
                    + "songusergender integer"
                    + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }

        public ArrayList<Song> readFromDB() {

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            Cursor c = db.query("songtable", null, null, null, null, null, null);
            data = new ArrayList<>();

            // ставим позицию курсора на первую строку выборки
            // если в выборке нет строк, вернется false
            if (c.moveToFirst()) {

                // определяем номера столбцов по имени в выборке
                int songnameColIndex = c.getColumnIndex("songname");
                int songgenreidColIndex = c.getColumnIndex("songgenreid");
                int songuserinfoColIndex = c.getColumnIndex("songuserinfo");
                int songusergenderColIndex = c.getColumnIndex("songusergender");

                do {
                    Song songData = new Song();
                    songData.songName = c.getString(songnameColIndex);
                    songData.songGenreID = c.getInt(songgenreidColIndex);
                    songData.songUserInfo = c.getString(songuserinfoColIndex);
                    songData.songUserGender = "1".equals(c.getString(songusergenderColIndex));
                    data.add(0, songData);
                } while (c.moveToNext());
            } else
                Log.d("songo", "0 rows");
            c.close();
            return data;
        }
    }

    private class parseTask extends AsyncTask<Void, Void, String> {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";
        String userid;
        ProgressDialog pdLoading = new ProgressDialog(MainActivity.this, R.style.MyDialogStyle);

        public parseTask(String parameters)
        {
            userid = parameters;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //метод работает в UI-потоке
            pdLoading.setMessage(getString(R.string.loading));
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL("http://songo.me.pn/db_GetFromSongs.php?userid=" + userid);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                resultJson = buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultJson;
        }

        @Override
        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);
            JSONObject dataJsonObj;
            Song songData;
            try {
                dataJsonObj = new JSONObject(strJson);
                JSONArray songs = dataJsonObj.getJSONArray("Song");
                for (int i = 0; i < songs.length(); i++) {
                    songData = new Song();
                    JSONObject song = songs.getJSONObject(i);
                    songData.songName = song.getString("song");
                    songData.songUserGender = "1".equals(song.getString("gender"));
                    //страна
                    Locale loc = new Locale("", song.getString("country"));
                    songData.songUserInfo = song.getString("age") + ", " + loc.getDisplayCountry();
                    //жанр
                    songData.songGenreID = song.getInt("GenreID");
                    ContentValues cv = new ContentValues();
                    SQLiteDatabase db = dbHelper.getWritableDatabase();
                    cv.put("songname", songData.songName);
                    cv.put("songgenreid", songData.songGenreID);
                    cv.put("songuserinfo", songData.songUserInfo);
                    cv.put("songusergender", songData.songUserGender);
                    long rowID = db.insert("songtable", null, cv);
                    if (rowID == -1)
                        new parseTask(userid).execute();
                    else {
                        mAdapter.insert(songData);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Snackbar.make(layout, R.string.connection_lost, Snackbar.LENGTH_LONG)
                        .show();
            }
            pdLoading.dismiss();
        }
    }
}

