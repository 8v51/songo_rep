package com.rainraingoaway.songov2;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Song> data = new ArrayList<>();
    //то, что ниже - для анимации
    private static final int UNSELECTED = -1;
    private RecyclerView recyclerView;
    private int selectedItem = UNSELECTED;

    public RecyclerAdapter(Context context, ArrayList<Song> data, RecyclerView recyclerView) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.recyclerView = recyclerView;
    }

    public void insert(Song song)
    {
        if (data.isEmpty()) {
            data.add(0, song);
            notifyDataSetChanged();
        }
        else
        {
            data.add(0, song);
            recyclerView.scrollToPosition(0);
            notifyItemInserted(0);
            notifyItemRangeChanged(0, data.size());
        }
    }

    public void swap(ArrayList<Song> datas){
        data.clear();
        data.addAll(datas);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_songName;
        TextView tv_songGenreID;
        TextView tv_songUserInfo;
        ExpandableLayout expandableLayout;
        private CardView expandButton;
        private ImageView albumArt;
        private ImageView youtubeButton;
        private ImageView shareButton;
        private ImageView yandexButton;
        private ImageView soundcloudButton;
        private ImageView spotifyButton;
        private ImageView genderImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_songName = (TextView) itemView.findViewById(R.id.tv_songName);
            tv_songGenreID = (TextView) itemView.findViewById(R.id.tv_songGenreID);
            tv_songUserInfo = (TextView) itemView.findViewById(R.id.tv_songUserInfo);
            albumArt = (ImageView) itemView.findViewById(R.id.album_art);
            youtubeButton = (ImageView) itemView.findViewById(R.id.youtube_button);
            yandexButton = (ImageView) itemView.findViewById(R.id.yandex_button);
            soundcloudButton = (ImageView) itemView.findViewById(R.id.soundcloud_button);
            spotifyButton = (ImageView) itemView.findViewById(R.id.spotify_button);
            shareButton = (ImageView) itemView.findViewById(R.id.share_button);
            genderImageView = (ImageView) itemView.findViewById(R.id.genderImage);
            expandableLayout = (ExpandableLayout) itemView.findViewById(R.id.expandable_layout);
            expandableLayout.setInterpolator(new OvershootInterpolator());
            expandButton = (CardView) itemView.findViewById(R.id.card_view);
            expandButton.setOnClickListener(this);
            youtubeButton.setOnClickListener(this);
            shareButton.setOnClickListener(this);
            yandexButton.setOnClickListener(this);
            spotifyButton.setOnClickListener(this);
            soundcloudButton.setOnClickListener(this);
        }

        public void bind(int position) {
            Song current = data.get(position);
            String songInfo = context.getString(R.string.genre) + GenrePicker.getGenreName(context, current.songGenreID);
            this.tv_songName.setText(current.songName);
            this.tv_songGenreID.setText(songInfo);
            this.tv_songUserInfo.setText(current.songUserInfo);
            if (!current.songUserGender)
                genderImageView.setImageResource(R.drawable.ic_gender_female);
            expandButton.setSelected(false);
            expandableLayout.collapse(false);
            new HTTPmaster.getAlbumArt(current.songName, albumArt).execute();
        }

        @Override
        public void onClick(View view) {
            String URL = data.get(getAdapterPosition()).songName.replaceAll(" ", "%20");
            switch (view.getId())
            {
                case R.id.youtube_button:
                    URL = "https://www.youtube.com/results?search_query=" + URL;
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
                    context.startActivity(browserIntent);
                    break;
                /*case R.id.vk_button:
                    URL = "http://www.m.vk.com/audio?q=" + URL;
                    Intent VKbrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
                    Toast.makeText(context, "Автоматический поиск в ВК временно не работает", Toast.LENGTH_LONG).show();
                    context.startActivity(VKbrowserIntent);
                    break;*/
                case R.id.yandex_button:
                    URL = "http://music.yandex.ru/search?text=" + URL;
                    Intent YandexBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
                    context.startActivity(YandexBrowserIntent);
                    break;
                case R.id.soundcloud_button:
                    URL = "http://soundcloud.com/search?q=" + URL;
                    Intent SoundCloudbrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
                    context.startActivity(SoundCloudbrowserIntent);
                    break;
                case R.id.spotify_button:
                    URL = "https://open.spotify.com/search/results/" + URL;
                    Intent SpotifybrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
                    context.startActivity(SpotifybrowserIntent);
                    break;
                case R.id.share_button:
                    String songName = data.get(getAdapterPosition()).songName;
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_TEXT, songName);
                    shareIntent.setType("text/plain");
                    context.startActivity(shareIntent);
                    break;
                default:
                    ViewHolder holder = (ViewHolder) recyclerView.findViewHolderForAdapterPosition(selectedItem);
                    if (holder != null) {
                        holder.expandButton.setSelected(false);
                        holder.expandableLayout.collapse();
                    }

                    if (getAdapterPosition() == selectedItem) {
                        selectedItem = UNSELECTED;
                    } else {
                        expandButton.setSelected(true);
                        expandableLayout.expand();
                        selectedItem = getAdapterPosition();
                    }
                    break;
            }

        }
    }
    
    // Создает новые views (вызывается layout manager-ом)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=inflater.inflate(R.layout.recycler_item, parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(position);
        setAnimation(holder.itemView, position);
        if (!data.get(position).songUserGender)
            holder.genderImageView.setImageResource(R.drawable.ic_gender_female);
    }

    private int lastPosition = -1;

    private void setAnimation(View viewToAnimate, int position) {
        // анимирует элементы, когда они впервые появляются на экране
        if (position > lastPosition) {
            Animation anim = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }
    // Возвращает размер данных (вызывается layout manager-ом)
    @Override
    public int getItemCount() {
        return data.size();
    }

}