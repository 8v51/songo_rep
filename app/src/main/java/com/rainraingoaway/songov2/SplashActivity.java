package com.rainraingoaway.songov2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sPref;
        sPref = getSharedPreferences("userinfo", MODE_PRIVATE);
        String checkPreferences = sPref.getString("genres", "");
        Intent intent;
        if(checkPreferences.isEmpty())
            intent = new Intent(this, StartScreen.class);
        else
            intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
