package com.rainraingoaway.songov2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class StartScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);
    }
    public void OnClickButton(View v){
        Intent intent = new Intent(this, UserInfo.class);
        startActivity(intent);
        finish();
    }
}
