package com.rainraingoaway.songov2;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.google.android.gms.iid.InstanceID;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

public class UserInfo extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Calendar calendar = Calendar.getInstance();
    Calendar cal = Calendar.getInstance();
    String formattedDate;
    String country_locale = "";//для сохранения страны в базе
    InputMethodManager imm;
    String instanceID = null;
    boolean[] genres = new boolean[14];//выбранные жанры
    boolean gender = true;//true - мужчина, false - женщина
    Spinner spinner;
    RadioButton radioMale;
    RadioButton radioFemale;
    CheckBox anyCheckBox;
    Button nextStep;
    DatePicker dp;
    Locale[] locales = Locale.getAvailableLocales();
    String[] ISOlocales = Locale.getISOCountries();
    SharedPreferences sPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().setTitle(R.string.userinfo_header);
        setContentView(R.layout.userinfo_layout);
        instanceID = InstanceID.getInstance(this).getId();
        radioMale = (RadioButton)findViewById(R.id.radioButton);
        radioFemale = (RadioButton)findViewById(R.id.radioButton2);
        nextStep = (Button)findViewById(R.id.nextStep);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        dp = (DatePicker) findViewById(R.id.dp);
        dp.init(calendar.get(calendar.YEAR),
                calendar.get(calendar.MONTH),
                calendar.get(calendar.DAY_OF_MONTH),
                new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                cal = Calendar.getInstance();
                cal.setTimeInMillis(0);
                cal.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                Date chosenDate = cal.getTime();

                // Форматирование даты
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                formattedDate = sdf.format(chosenDate);
            }
        });

        ArrayList<String> countries = new ArrayList<>();
        for (Locale locale : locales) {
            String country = locale.getDisplayCountry();
            if (country.trim().length()>0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries);
        String current = this.getResources().getConfiguration().locale.getDisplayCountry();
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.spinner_row, countries);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        int country_code = 0;//для выбора в спиннере
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(current)){
                country_code = i;
                break;
            }
        }
        final int for_selection = country_code;//если не final, то не передастся в Runnable
        spinner.post(new Runnable() {
            public void run() {
                spinner.setSelection(for_selection);
            }
        });

        //проверка, имеются ли сохраненные данные
        SharedPreferences prefs = this.getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        if (prefs.contains("gender"))
        {
            getSupportActionBar().setTitle(R.string.edit_profile);
            instanceID = prefs.getString("InstanceID", null);
            nextStep.setText(R.string.ready);

            //жанры
            String genresarray = prefs.getString("genres", null);
            char genrechar;
            for (int i = 0; i < 14; i++)
            {
                genrechar = genresarray.charAt(i);
                if (genrechar == '1') {
                    anyCheckBox = (CheckBox) findViewById(GenrePicker.getCheckboxId(i));
                    anyCheckBox.setChecked(true);
                    genres[i] = true;
                }
            }

            //дата
            String[] dateparts = prefs.getString("dateOfBirth", null).split("-");
            dp.updateDate(Integer.parseInt(dateparts[0]),
                    Integer.parseInt(dateparts[1]) - 1,//месяца в спиннере начинаются с 0, поэтому -1
                    Integer.parseInt(dateparts[2]));

            //пол
            gender = prefs.getBoolean("gender", true);
            if (gender)
            {
                radioMale.setChecked(true);
                radioFemale.setChecked(false);
            }
            else
            {
                radioMale.setChecked(false);
                radioFemale.setChecked(true);
            }
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nextStep:
                boolean genresChecked = false;
                for (int i = 0; i < 14; i++){
                    if(genres[i])
                        genresChecked = true;
                }
                if(cal.get(Calendar.YEAR) > 2013){
                    Snackbar.make(v, R.string.wrong_year, Snackbar.LENGTH_LONG)
                            .show();
                } else {
                    if (genresChecked) {
                        StringBuilder genresBoolString = new StringBuilder();//переводим массив с жанрами в строку, чтобы сохранить в preferences
                        int localgenreid;
                        for (boolean i : genres) {
                            localgenreid = (Boolean.compare(i, false));
                            genresBoolString.append(String.valueOf(localgenreid));
                        }
                        sPref = getSharedPreferences("userinfo", MODE_PRIVATE);
                        SharedPreferences.Editor ed = sPref.edit();
                        ed.putString("InstanceID", instanceID);
                        ed.putString("country", country_locale);
                        ed.putBoolean("gender", gender);
                        ed.putString("genres", genresBoolString.toString());
                        ed.putString("dateOfBirth", formattedDate);
                        ed.apply();
                        StringBuilder parameters = new StringBuilder();
                        parameters.append("genreid=");
                        parameters.append(genresBoolString);
                        parameters.append("&instanceid=");
                        parameters.append(instanceID);
                        parameters.append("&country=");
                        parameters.append(country_locale);
                        parameters.append("&gender=");
                        int genderInt = (Boolean.compare(gender, false));
                        parameters.append(String.valueOf(genderInt));
                        parameters.append("&dateofbirth=");
                        parameters.append(formattedDate);

                        new HTTPmaster.postTask().execute("http://songo.me.pn/db_CreateRowUsers.php",
                                parameters.toString());
                        Intent intent = new Intent(this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Snackbar.make(v, R.string.genres_not_chosen, Snackbar.LENGTH_LONG)
                                .show();
                    }
                }
                break;
        }
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.radioButton:
                if (checked) {
                    radioMale.setChecked(true);
                    radioFemale.setChecked(false);
                    gender = true;
                }
                break;
            case R.id.radioButton2:
                if (checked) {
                    radioMale.setChecked(false);
                    radioFemale.setChecked(true);
                    gender = false;
                }
                break;
        }
    }

    public void onCheckboxClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();
        genres[GenrePicker.getGenreId(view.getId())] = checked;
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                                   int pos, long id) {
        for (String locale : ISOlocales){
            Locale obj = new Locale("", locale);
            if (obj.getDisplayCountry().equals(spinner.getItemAtPosition(pos).toString())) {
                country_locale = obj.getCountry();
                break;
            }
        }
    }

    public void onNothingSelected(AdapterView<?> parent) {
    }
}
